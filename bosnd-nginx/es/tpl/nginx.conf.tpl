worker_processes auto;

events {
  worker_connections  4096;
}

http {

    {{range $index, $entry := .Services}}
    upstream {{$entry.Name}}_us {
        hash $remote_addr;
        {{range $index, $ep := $entry.Endpoints -}}
        server {{$ep.Address}}:{{$entry.Labels.port}}; {{end}}
    }

    server {
        listen 80;
        location / {
                proxy_pass http://{{$entry.Name}}_us;

                proxy_set_header   Host             $host;
                proxy_set_header   X-Forwarded-Server $host;
                proxy_set_header   X-Real-IP        $remote_addr;
                proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
                proxy_set_header   X-Forwarded-Proto $scheme;
        }

    }
    {{ end }}
}

