# Docker Meetup Loadbalancer Stuff

# MobaXterm
https://mobaxterm.mobatek.net/

# swarm-1.yml
- simpliest swarm file

# swarm-3.yml
- simple port expose
https://docs.docker.com/engine/swarm/ingress/

# swarm-5.yml
- simple port expose
- replicas

docker service inspect --format="{{json .Endpoint.Spec.Ports}}" test_app

# swarm-7.yml
docker network create --driver=overlay --attachable test_net
docker stack deploy -c swarm-7.yml test

docker stack ls
docker service ls
docker service ps test_app

docker run -ti --net=test_net -p 80:80 -v /var/run/docker.sock:/var/run/docker.sock:ro -e 'TZ=/usr/share/zoneinfo/Europe/Vienna' bosnd-nginx bash

docker run -d --net=test_net -p 80:80 -v /var/run/docker.sock:/var/run/docker.sock:ro -e 'TZ=/usr/share/zoneinfo/Europe/Vienna' bosnd-nginx

/bosnd/bin/bosnd run -c /bosnd/conf/bosnd.yml

# swarm-8.yml

~~~
https://docs.traefik.io/user-guide/swarm-mode/
~~~

docker swarm init

docker network create --driver overlay --attachable test_net

docker stack deploy -c swarm-8.yml test

docker service create --name traefik --constraint=node.role==manager --publish 80:80 --publish 8080:8080 --mount type=bind,source=/var/run/docker.sock,target=/var/run/docker.sock --network test_net  traefik --docker --docker.swarmMode --docker.domain=eu-central-1.compute.amazonaws.com  --docker.watch --api

## Test it

http://ec2-35-157-194-243.eu-central-1.compute.amazonaws.com:8080/dashboard/

curl -H Host:test-app.eu-central-1.compute.amazonaws.com http://127.0.0.1


# swarm-10.yml
- with network
- with labels
- with entrypoint

